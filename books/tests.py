from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import os
import time
import random
import string

from faker import Faker
from books.models import Books


class LandingPageUnitTest(TestCase):
    def test_landing_page_found(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_model_string_return(self):
        letters = string.ascii_letters
        random_book_id = ''.join(random.choice(letters) for i in range(10))
        random_like_count = random.randint(1,10)
        faker = Faker()
        title = faker.text()
        publisher = faker.name()
        Books.objects.create(
            book_id = random_book_id,
            title = title,
            publisher = publisher,
            published_date = None,
            like_count = random_like_count,
            image_link = '',
            authors = None,
            average_rating = None
        )

        data = Books.objects.get(title=title)
        self.assertEqual(str(data), title + ' - ' + random_book_id)

class LandingPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(LandingPageFunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(LandingPageFunctionalTest, self).tearDown()

    def test_search_box_show_books_with_name(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)
        
        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        query_input.send_keys("Web Programming (Client Side and Server Side)")
        time.sleep(15) # Wait web page to process and showing result

        book_item = self.browser.find_element_by_class_name('book-item')
        book_data = book_item.get_attribute('innerHTML')

        self.assertIn('Web', book_data)

        book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(15)
        liked_book_data = Books.objects.get(title=book_title)
        self.assertEqual(liked_book_data.like_count, 1)        
        
    def test_modal_showing_most_liked_book(self):
        books_added = []
        # Create 10 book instance
        for i in range(10):
            letters = string.ascii_letters
            random_book_id = ''.join(random.choice(letters) for i in range(10))
            random_like_count = random.randint(1,10)
            faker = Faker()
            title = faker.text()
            publisher = faker.name()
            Books.objects.create(
                book_id = random_book_id,
                title = title,
                publisher = publisher,
                published_date = None,
                like_count = random_like_count,
                image_link = '',
                authors = None,
                average_rating = None
            )

            books_added.append((title, random_like_count))

        books_added = sorted(books_added, key = lambda books : books[1], reverse = True)
        top_5_book = books_added[0:5]
        self.browser.get(self.live_server_url + '/')
        time.sleep(20)
        
        modal_button = self.browser.find_element_by_id('modalButton')
        modal_button.click()
        time.sleep(15)

        modal_data = self.browser.find_element_by_id('topBookModal').get_attribute('innerHTML')
        for x in top_5_book:
            self.assertTrue(x[0] in modal_data)
        
        for i in range(len(top_5_book) -1 ):
            self.assertTrue(modal_data.index(top_5_book[i][0]) < modal_data.index(top_5_book[i+1][0]))
    
    def test_books_without_full_data(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)

        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        query_input.send_keys("Bluetongue - AGDPT, CFT, Microtitre Neutralisation Test")
        query_input.send_keys(" ")
        time.sleep(15)

        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)
        
        books = Books.objects.get(title='Bluetongue - AGDPT, CFT, Microtitre Neutralisation Test')

        self.assertTrue(books.like_count , 1)

        query_input.clear()
        query_input.send_keys("Counsel to Ladies and Easy-going Men on Their Business Investments")
        query_input.send_keys(" ")
        time.sleep(15)

        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)
        
        books = Books.objects.get(title='Counsel to Ladies and Easy-going Men on Their Business Investments')

        self.assertTrue(books.like_count , 1)

        query_input.clear()
        query_input.send_keys("Manajemen Fit & Proper Test")
        query_input.send_keys(" ")
        time.sleep(15)

        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)
        
        books = Books.objects.get(title='Manajemen Fit & Proper Test')

        self.assertTrue(books.like_count , 1)
        self.browser.execute_script("localStorage.clear();")

        self.browser.get(self.live_server_url + '/')
        time.sleep(5)

        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        query_input.send_keys("Manajemen Fit & Proper Test")
        query_input.send_keys(" ")
        time.sleep(15)

        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)
        
        books = Books.objects.get(title='Manajemen Fit & Proper Test')

        self.assertTrue(books.like_count , 2)

    def test_unlike_book(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)

        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        query_input.send_keys("Harry Potter dan tawanan Azkaban gramedia pustaka utama")
        time.sleep(2)
        query_input.send_keys(" ")
        time.sleep(15)

        book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)
        
        books = Books.objects.get(title='Harry Potter dan tawanan Azkaban')
        self.assertEqual(books.like_count , 1)

        book_like_button.click()
        time.sleep(5)
        
        books = Books.objects.get(title='Harry Potter dan tawanan Azkaban')
        self.assertEqual(books.like_count , 0)

    def test_unlike_book_not_exist(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)

        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        query_input.send_keys("Harry Potter dan tawanan Azkaban gramedia")
        time.sleep(2)
        query_input.send_keys(" ")
        time.sleep(15)

        book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)
        
        # Deleted in case of failed db migrations or db change
        books = Books.objects.get(title='Harry Potter dan tawanan Azkaban').delete()

        book_like_button.click()
        time.sleep(15)
        
        book_like_button = self.browser.find_element_by_class_name('book-like-button').get_attribute('innerHTML')
        self.assertEqual(book_like_button , 'Error unlike book')

    def test_like_from_top_books(self):
        books_added = []
        # Create 10 book instance
        for i in range(10):
            letters = string.ascii_letters
            random_book_id = ''.join(random.choice(letters) for i in range(10))
            random_like_count = random.randint(1,10)
            faker = Faker()
            title = faker.text()
            publisher = faker.name()
            Books.objects.create(
                book_id = random_book_id,
                title = title,
                publisher = publisher,
                published_date = None,
                like_count = random_like_count,
                image_link = '',
                authors = None,
                average_rating = None
            )

            books_added.append((title, random_like_count))

        books_added = sorted(books_added, key = lambda books : books[1], reverse = True)
        top_5_book = books_added[0:5]

        before_like_count = Books.objects.get(title=top_5_book[0][0]).like_count
        
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)
        top_book_button = self.browser.find_element_by_id('modalButton')
        top_book_button.click()
        time.sleep(5)

        like_button_top_book = self.browser.find_element_by_xpath("//div[@id='topBookContainer']/div[1]/div/button")
        like_button_top_book.click()
        time.sleep(5)

        after_like_count = Books.objects.get(title=top_5_book[0][0]).like_count

        self.assertTrue(after_like_count == before_like_count+1)
